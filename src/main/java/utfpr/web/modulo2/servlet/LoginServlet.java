/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.web.modulo2.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *  This servlet is used to process all the sign up in the system
 * 
 * @author Lucas Cruz
 */
@WebServlet(name = "Login Servlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet  {
    
      /**
     * Processa requisições HTTP para os métodos <code>GET</code> e
     * <code>POST</code>.
     *
     * @param request Requisição
     * @param response Resposta
     * @throws ServletException Se ocorrer um exceção específica de Servlet
     * @throws IOException Se ocorrer uma exceção de E/S
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         response.setContentType("text/html;charset=UTF-8");

        if (request.getMethod().equalsIgnoreCase("post")) {
            String login = request.getParameter("login");
            String senha = request.getParameter("senha");
            String perfil = request.getParameter("perfil");
            if(login.equals(senha)){
                //manda pro servlet de sucesso
                response.sendRedirect("/home/sucesso?login="+login+"&perfil="+perfil);
            } else {
                //manda pra pagina de erro
                response.sendRedirect("erro.xhtml");
            }
            
        }
    }
    
}
